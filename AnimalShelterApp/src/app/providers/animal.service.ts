import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Animal } from '../models/animal.model';

@Injectable({
  providedIn: 'root'
})
export class AnimalService {
  url = "https://localhost:7247/api/";
  constructor(private http: HttpClient) { }
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      // 'Referrer-Policy': 'strict-origin-when-cross-origin',
    }),
  };
  getAnimals(): Observable<Animal[]> {
    return this.http
      .get(this.url + "Animals", this.httpOptions)
      .pipe(map((res) => <Animal[]>res));
  }
  getAnimalsByOwner(owner: string): Observable<Animal[]> {
    return this.http
      .get(this.url + "Animals/" + owner +"/owner", this.httpOptions)
      .pipe(map((res) => <Animal[]>res));
  }
  getAnimalById(id: number): Observable<Animal> {
    return this.http
      .get(this.url + "Animals/" + id, this.httpOptions)
      .pipe(map((res) => <Animal>res));
  }
  postNewAnimal(Animal: Animal): Observable<Animal> {
    return this.http.post<Animal>(this.url + 'Animals',
      {
        animalId: Animal.animalId,
        name: Animal.name,
        species: Animal.species,
        breed: Animal.breed,
        owner: Animal.owner,
        checkInDate: Animal.checkInDate,
        checkOutDate: Animal.checkOutDate
      },
      this.httpOptions);
  }
  updateAnimal(Animal: Animal): Observable<Animal> {
    return this.http.put<Animal>(this.url + 'Animals/' + Animal.animalId,
      {
        animalId: Animal.animalId,
        name: Animal.name,
        species: Animal.species,
        breed: Animal.breed,
        owner: Animal.owner,
        checkInDate: Animal.checkInDate,
        checkOutDate: Animal.checkOutDate
      },
      this.httpOptions);
  }
  deleteAnimal(Animal: Animal): Observable<Animal> {
    return this.http.delete<Animal>(this.url + 'Animals/' + Animal.animalId,
      this.httpOptions);
  }
}