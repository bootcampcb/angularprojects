import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  message: string = 'under construction';
  intro: string = "We offer a variety of services for your furry/feathered/scaled friends, including:";
  details: string = "Vestibulum elementum lacinia nibh. Vestibulum id efficitur sapien. Aenean quam ante, lobortis sit amet arcu vitae, consequat congue justo. Donec dictum nibh non nisl laoreet pharetra. Cras quis laoreet orci. Nullam nisl nisl, condimentum eu est id, finibus vestibulum augue. Nullam dictum consectetur est, id luctus neque eleifend eu.";
  conclusion: string = "Thank you for choosing the Animal Hotel for your pet's needs!";
}
