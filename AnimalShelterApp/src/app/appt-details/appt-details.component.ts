import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Animal } from '../models/animal.model';
import { AnimalService } from '../providers/animal.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-appt-details',
  templateUrl: './appt-details.component.html',
  styleUrls: ['./appt-details.component.css']
})
export class ApptDetailsComponent {
  animal!: Animal;
  animalId: number = 0;

  constructor(private activatedRoute: ActivatedRoute, private AnimalService: AnimalService, private router: Router) { }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.animalId = params['id']});
    this.AnimalService.getAnimalById(this.animalId).subscribe((data) => {
      this.animal = data;
    });
  }
  onClickUpdateButton() {
    this.router.navigate(['/appt-update'], {queryParams: {id: this.animalId}});
  }
}
