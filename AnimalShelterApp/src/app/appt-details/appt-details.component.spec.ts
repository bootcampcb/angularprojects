import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApptDetailsComponent } from './appt-details.component';

describe('ApptDetailsComponent', () => {
  let component: ApptDetailsComponent;
  let fixture: ComponentFixture<ApptDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApptDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ApptDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
