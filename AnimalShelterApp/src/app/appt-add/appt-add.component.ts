import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Animal } from '../models/animal.model';
import { AnimalService } from '../providers/animal.service';

@Component({
  selector: 'app-appt-add',
  templateUrl: './appt-add.component.html',
  styleUrls: ['./appt-add.component.css']
})
export class ApptAddComponent {
  name: string = "";
  species: string = "";
  breed: string = "";
  owner: string = "";
  checkInDate: string = "";
  checkOutDate: string = "";
  constructor(private activatedRoute: ActivatedRoute, private AnimalService: AnimalService, private router: Router) {

  }

  onClickAddButton() {
    this.AnimalService.postNewAnimal(new Animal(0, this.name, this.species, this.breed, this.owner, this.checkInDate, this.checkOutDate)).subscribe(data => console.dir(data));
    this.router.navigate(['/appts']);
  }
}
