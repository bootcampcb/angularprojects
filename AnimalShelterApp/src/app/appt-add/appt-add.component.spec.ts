import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApptAddComponent } from './appt-add.component';

describe('ApptAddComponent', () => {
  let component: ApptAddComponent;
  let fixture: ComponentFixture<ApptAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApptAddComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ApptAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
