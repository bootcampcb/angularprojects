import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Animal } from '../models/animal.model';
import { AnimalService } from '../providers/animal.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-appt-update',
  templateUrl: './appt-update.component.html',
  styleUrls: ['./appt-update.component.css']
})
export class ApptUpdateComponent {
  animal!: Animal;
  animalId: number = 0;
  name: string = "";
  species: string = "";
  breed: string = "";
  owner: string = "";
  checkInDate: string = "";
  checkOutDate: string = "";
  constructor(private activatedRoute: ActivatedRoute, private AnimalService: AnimalService, private router: Router) {

  }
  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.animalId = params['id']});
    this.AnimalService.getAnimalById(this.animalId).subscribe((data) => {
      this.animal = data;
      this.name = data.name;
      this.species = data.species;
      this.breed = data.breed;
      this.owner = data.owner;
      this.checkInDate = data.checkInDate.split("T")[0];
      this.checkOutDate = data.checkOutDate.split("T")[0];
    });
  }
  onClickSaveButton() {
    this.AnimalService.updateAnimal(new Animal(this.animalId, this.name, this.species, this.breed, this.owner, this.checkInDate, this.checkOutDate)).subscribe(data => console.dir(data));
    alert("Record updated");
    this.router.navigate(['/appts']);
  }
}

