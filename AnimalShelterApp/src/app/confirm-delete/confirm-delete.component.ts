import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Animal } from '../models/animal.model';
import { AnimalService } from '../providers/animal.service';

@Component({
  selector: 'app-confirm-delete',
  templateUrl: './confirm-delete.component.html',
  styleUrls: ['./confirm-delete.component.css']
})
export class ConfirmDeleteComponent {

  animal!: Animal;
  animalId: number = 0;
  name: string = "";
  owner: string = "";
  checkInDate: string = "";
  checkOutDate: string = "";

  constructor(private activatedRoute: ActivatedRoute, private AnimalService: AnimalService, private router: Router) {
  }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.animalId = params['id']});
      this.AnimalService.getAnimalById(this.animalId).subscribe((data) => {
        this.animal = data;
        this.name = this.animal.name;
        this.owner = this.animal.owner;
        this.checkInDate = this.animal.checkInDate.split("T")[0];
        this.checkOutDate = this.animal.checkOutDate.split("T")[0];
      });
  }
  onClickDeleteButton() {
    this.AnimalService.deleteAnimal(this.animal).subscribe(data => console.dir(data));
    alert("Record deleted");
    this.router.navigate(['/appts']);
  }
}
