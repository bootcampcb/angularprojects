import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Animal } from '../models/animal.model';
import { AnimalService } from '../providers/animal.service';

@Component({
  selector: 'app-appts',
  templateUrl: './appts.component.html',
  styleUrls: ['./appts.component.css']
})
export class ApptsComponent {
  animals: Array<Animal> = [];
  owner: string = "";

  constructor(private activatedRoute: ActivatedRoute, private AnimalService: AnimalService) { }

  ngOnInit(): void {
    this.AnimalService.getAnimals().subscribe((data) => {
      this.animals = data;
    });
  }
  onClickSearchButton() {
    this.AnimalService.getAnimalsByOwner(this.owner).subscribe((data) => {
      this.animals = data;
    });
//    this.router.navigate(['/appts']);
  }
}
