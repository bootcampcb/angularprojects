import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ApptsComponent } from './appts/appts.component';
import { ApptDetailsComponent } from './appt-details/appt-details.component';
import { ApptAddComponent } from './appt-add/appt-add.component';
import { ConfirmDeleteComponent } from './confirm-delete/confirm-delete.component';
import { HomeComponent } from './home/home.component';
import { ApptUpdateComponent } from './appt-update/appt-update.component';
import { AnimalService } from './providers/animal.service';
import { FooterComponent } from './footer/footer.component';
import { ApplicationComponent } from './application/application.component';

const appRoutes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: "home", title: 'Animal Hotel', component: HomeComponent },
  { path: "appts", title: 'Appointments', component: ApptsComponent },
  { path: "appt-details", title: 'Details', component: ApptDetailsComponent },
  { path: "appt-add", title: 'Add Appointment', component: ApptAddComponent },
  { path: "confirm-delete", title: 'Confirm Delete', component: ConfirmDeleteComponent },
  { path: "appt-update", title: 'Update Appointment', component: ApptUpdateComponent},
  { path: "application", title: 'Animal Hotel Application', component: ApplicationComponent},
]
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ApptsComponent,
    ApptDetailsComponent,
    ApptAddComponent,
    ConfirmDeleteComponent,
    HomeComponent,
    ApptUpdateComponent,
    FooterComponent,
    ApplicationComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    HttpClientModule,
    FontAwesomeModule,
    
  ],
  providers: [AnimalService],
  bootstrap: [AppComponent]
})
export class AppModule { }
