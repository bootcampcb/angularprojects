import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { PlaylistComponent } from './playlist/playlist.component';
import { PlayerComponent } from './player/player.component';
import { YouTubePlayerModule } from "@angular/youtube-player";

const appRoutes: Routes = [
  { path: "", component: PlaylistComponent},
  { path: "playlist", component: PlaylistComponent },
  { path: "player", component: PlayerComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    PlaylistComponent,
    PlayerComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    YouTubePlayerModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
