import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.css']
})
export class PlaylistComponent implements OnInit {

  songs = [
    {
      id: 1,
      name: 'Billie Jean',
      artist: 'Michael Jackson'
    },
    {
      id: 2,
      name: "Livin' on a Prayer",
      artist: 'Bon Jovi'
    },
    {
      id: 3,
      name: "Sweet Child o' Mine",
      artist: "Guns N' Roses",
    },
    { id: 4, name: 'Take On Me', artist: 'a-ha' },
    {
      id: 5,
      name: 'Like a Virgin',
      artist: 'Madonna',
    }
  ]

  constructor(private router: Router) {}

  ngOnInit() { }

  playSong(id: string): void {
    this.router.navigate(['/player'],
      {
        queryParams: {
          id: id
        }
      });
  }

}