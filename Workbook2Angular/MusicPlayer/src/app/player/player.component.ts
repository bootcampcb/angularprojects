import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css'],
})
export class PlayerComponent {
  song!: any;
  url!: SafeResourceUrl;
  videoId!: string;
  songs = [
    {
      id: 1,
      name: 'Billie Jean',
      artist: 'Michael Jackson',
      src: 'https://www.youtube.com/embed/Zi_XLOBDo_Y',
    },
    {
      id: 2,
      name: "Livin' on a Prayer",
      artist: 'Bon Jovi',
      src: 'https://www.youtube.com/embed/lDK9QqIzhwk',
    },
    {
      id: 3,
      name: "Sweet Child o' Mine",
      artist: "Guns N' Roses",
      src: 'https://www.youtube.com/embed/1w7OgIMMRc4',
    },
    {
      id: 4,
      name: 'Take On Me',
      artist: 'a-ha',
      src: 'https://www.youtube.com/embed/djV11Xbc914',
    },
    {
      id: 5,
      name: 'Like a Virgin',
      artist: 'Madonna',
      src: 'https://www.youtube.com/embed/s__rX_WL100',
    },
  ];
  // songs = [
  //   {
  //     id: 1,
  //     name: 'Billie Jean',
  //     artist: 'Michael Jackson',
  //     src: 'https://www.youtube.com/watch?v=Zi_XLOBDo_Y',
  //   },
  //   {
  //     id: 2,
  //     name: "Livin' on a Prayer",
  //     artist: 'Bon Jovi',
  //     src: 'https://www.youtube.com/watch?v=lDK9QqIzhwk',
  //   },
  //   {
  //     id: 3,
  //     name: "Sweet Child o' Mine",
  //     artist: "Guns N' Roses",
  //     src: 'https://www.youtube.com/watch?v=1w7OgIMMRc4',
  //   },
  //   {
  //     id: 4,
  //     name: 'Take On Me',
  //     artist: 'a-ha',
  //     src: 'https://www.youtube.com/watch?v=Ktb44YLL8Nw',
  //   },
  //   {
  //     id: 5,
  //     name: 'Like a Virgin',
  //     artist: 'Madonna',
  //     src: 'https://www.youtube.com/watch?v=UHXGc2oWyJ4',
  //   },
  // ];
  constructor(
    private activatedRoute: ActivatedRoute,
    private sanitizer: DomSanitizer) {

  }
  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params) => {
      let id = params['id'];
      this.song = this.getSong(id);
      this.url =
        this.sanitizer.bypassSecurityTrustResourceUrl(this.song.src);
      this.videoId = this.song.src.substring(this.song.src.indexOf("=") + 1);
    });
  }
  getSong(id: number) {
    return this.songs.find((song) => song.id == id);
  }
}
