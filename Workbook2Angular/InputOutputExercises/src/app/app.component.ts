import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'InputOutputExercises';
  books = ['Book 1', 'Book 2', 'Book 3'];
  book: string = "";
  clickEdit(title: string) {
    this.book = `Editing book: ${title}`;
  }
}
