import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Course } from '../models/course.model';
import { CourseService } from '../providers/course.service';

@Component({
  selector: 'app-course-delete',
  templateUrl: './course-delete.component.html',
  styleUrls: ['./course-delete.component.css']
})
export class CourseDeleteComponent {
  course!: Course;
  id: number = 0;
  // dept: string = "";
  courseNum: number = 0;
  courseName: string = "";
  // instructor: string = "";
  // startDate: string = "";
  // numDays: number = 0;

  constructor(private activatedRoute: ActivatedRoute, private CourseService: CourseService, private router: Router) {
  }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.id = params['id']});
      this.CourseService.getCourseById(this.id).subscribe((data) => {
        this.course = data;
        this.courseNum = this.course.courseNum;
        this.courseName = this.course.courseName;
        // this.completed = this.todo.completed;
      });
  }
  onClickDeleteButton() {
    this.CourseService.deleteCourse(this.course).subscribe(data => console.dir(data));
    alert("Record deleted");
    this.router.navigate(['/courses']);
  }
}
