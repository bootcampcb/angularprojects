import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Course } from '../models/course.model';
import { CourseService } from '../providers/course.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-course-update',
  templateUrl: './course-update.component.html',
  styleUrls: ['./course-update.component.css']
})

export class CourseUpdateComponent {
  course!: Course;
  id: number = 0;
  dept: string = "";
  courseNum: number = 0;
  courseName: string = "";
  instructor: string = "";
  startDate: string = "";
  numDays: number = 0;

  constructor(private activatedRoute: ActivatedRoute, private CourseService: CourseService, private router: Router) { }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.id = params['id']});
    this.CourseService.getCourseById(this.id).subscribe((data) => {
      this.course = data;
      this.dept = data.dept;
      this.courseNum = data.courseNum;
      this.courseName = data.courseName;
      this.instructor = data.instructor;
      this.startDate = data.startDate;
      this.numDays = data.numDays;
    });
  }
  onClickSaveButton() {
    this.CourseService.updateCourse(new Course(this.id, this.dept, this.courseNum, this.courseName, this.instructor, this.startDate, this.numDays)).subscribe(data => console.dir(data));
    alert("Record updated");
    this.router.navigate(['/courses']);
  }
}
