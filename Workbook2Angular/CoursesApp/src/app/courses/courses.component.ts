import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Course } from '../models/course.model';
import { CourseService } from '../providers/course.service';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent {
  courses: Array<Course> = [];

  constructor(private activatedRoute: ActivatedRoute, private CourseService: CourseService) { }

  ngOnInit(): void {
    this.CourseService.getCourses().subscribe((data) => {
      this.courses = data;
    });
  }
}
