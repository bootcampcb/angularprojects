import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Course } from '../models/course.model';

@Injectable({
  providedIn: 'root'
})
export class CourseService {
  constructor(private http: HttpClient) {}
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Referrer-Policy': 'strict-origin-when-cross-origin',
    }),
  };

  getCourses(): Observable<Course[]> {
    return this.http
      .get('http://localhost:8081/api/courses', this.httpOptions)
      .pipe(map((res) => <Course[]>res));
  }
  getCourseById(id: number): Observable<Course> {
    return this.http
      .get('http://localhost:8081/api/courses/'+ id, this.httpOptions)
      .pipe(map((res) => <Course>res));
  }
  postNewCourse(Course: Course): Observable<Course> {
    return this.http.post<Course>('http://localhost:8081/api/courses',
      {
        id: Course.id,
        dept: Course.dept,
        courseNum: Course.courseNum,
        courseName: Course.courseName,
        instructor: Course.instructor,
        startDate: Course.startDate,
        numDays: Course.numDays
      },
      this.httpOptions);
  }
  updateCourse(Course: Course): Observable<Course> {
    return this.http.put<Course>('http://localhost:8081/api/courses/' + Course.id,
      {
        courseNum: Course.courseNum,
        courseName: Course.courseName,
        instructor: Course.instructor,
        startDate: Course.startDate,
        numDays: Course.startDate
      },
      this.httpOptions);
  }
  deleteCourse(Course: Course): Observable<Course> {
    return this.http.delete<Course>('http://localhost:8081/api/courses/' + Course.id,
      this.httpOptions);
  }
}
