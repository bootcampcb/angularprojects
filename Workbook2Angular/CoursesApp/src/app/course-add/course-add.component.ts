import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Course } from '../models/course.model';
import { CourseService } from '../providers/course.service';

@Component({
  selector: 'app-course-add',
  templateUrl: './course-add.component.html',
  styleUrls: ['./course-add.component.css']
})
export class CourseAddComponent {
  dept: string = "";
  courseNum: number = 0;
  courseName: string = "";
  instructor: string = "";
  startDate: string = "";
  numDays: number = 0;
  constructor(private activatedRoute: ActivatedRoute, private CourseService: CourseService, private router: Router) {

  }

  onClickAddButton() {
    this.CourseService.postNewCourse(new Course(0, this.dept, this.courseNum, this.courseName, this.instructor, this.startDate, this.numDays)).subscribe(data => console.dir(data));
    this.router.navigate(['/courses']);
  }
}
