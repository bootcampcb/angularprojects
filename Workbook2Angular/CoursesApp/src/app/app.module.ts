import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { CoursesComponent } from './courses/courses.component';
import { CourseDetailsComponent } from './course-details/course-details.component';
import { CourseService } from './providers/course.service';
import { CourseAddComponent } from './course-add/course-add.component';
import { CourseDeleteComponent } from './course-delete/course-delete.component';
import { CourseUpdateComponent } from './course-update/course-update.component';

const appRoutes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: "home", component: HomeComponent },
  { path: "courses", component: CoursesComponent },
  { path: "course-details", component: CourseDetailsComponent },
  { path: "course-add", component: CourseAddComponent },
  { path: "course-delete", component: CourseDeleteComponent },
  { path: "course-update", component: CourseUpdateComponent},
]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    CoursesComponent,
    CourseDetailsComponent,
    CourseAddComponent,
    CourseDeleteComponent,
    CourseUpdateComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    HttpClientModule,
  ],
  providers: [CourseService],
  bootstrap: [AppComponent]
})
export class AppModule { }
