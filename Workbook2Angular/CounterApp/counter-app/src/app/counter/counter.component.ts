import { Component } from '@angular/core';
@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.css']
})
export class CounterComponent {
  count = 0;
  totalCount = 0
  increment() {
    this.count++;
    this.totalCount++;
  }
  decrement() {
    this.count--;
    this.totalCount++;
  }
}