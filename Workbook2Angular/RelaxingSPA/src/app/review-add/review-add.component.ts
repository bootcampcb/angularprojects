import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Reviews } from '../models/reviews.model';
import { ReviewsService } from '../providers/reviews.service';

@Component({
  selector: 'app-review-add',
  templateUrl: './review-add.component.html',
  styleUrls: ['./review-add.component.css']
})
export class ReviewAddComponent {
  d = new Date();
  reviewer: string = "";
  date: string = this.d.toLocaleDateString();;
  comment: string = "";

  constructor(private activatedRoute: ActivatedRoute, private reviewsService: ReviewsService) {
  }

  onSubmit() {
    this.reviewsService.postNewReview(new Reviews(this.date, this.reviewer, this.comment)).subscribe(data => console.dir(data));
  }
}
