import { Component } from '@angular/core';
import { Reviews } from '../models/reviews.model';
import { ReviewsService } from '../providers/reviews.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  intro: string = "Relaxing SPA has been a trusted source of effective body work and massage therapy for many years. We pride ourselves on providing practical knowledge and helpful recommendations dispensed by a team of professionally trained and certified massage practitioners committed to offering long term relief for a range of physical and psychological ailments.";
  details: string = "We believe that therapeutic touch, when professionally applied in one of its many established forms, has the power to restore and activate the healing potential present within each individual. Our certified massage therapists have provided beneficial results that range from restoring mobility to those suffering with physical ailments, to helping clients overcome debilitating stress related conditions.";
  conclusion: string = "Relaxing SPA's therapeutic approach is to welcome you each and every time like it is your first time and feel a part of our Relaxing SPA Family. We will aim to match you with one of our experienced & talented therapists who will customize your massage using a variety of modalities and techniques; to focus on your specific needs to ensure you receive the highest quality and most appropriate treatment to unite your body, mind and soul. Relaxing SPA strives not only to meet your expectations but to exceed them; so you will value yourself & Relaxing SPA enough to make massage a regular part of your health & well-being maintenance plan.";
  reviews!: Array<Reviews>;

  constructor(private reviewService: ReviewsService) { }

  ngOnInit(): void {
    this.reviewService.getReviews().subscribe((data) => {
      this.reviews = data});
  }

}
