import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Categories } from '../models/categories.model';
import { CategoriesService } from '../providers/categories.service';
import { Services } from '../models/services.model';
import { ServicesService } from '../providers/services.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent {
  categories!: Array<Categories>;
  category!: any;
  services!: Array<Services>;
  matchingServices: Array<any> = [];
  image: string = "";

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private CategoriesService: CategoriesService, private ServicesService: ServicesService) { }

  ngOnInit(): void {
    this.CategoriesService.getCategories().subscribe((data) => {
      this.categories = data});
    this.ServicesService.getServices().subscribe((data) => {
      this.services = data});
  }

  onSelectCat(event: any) : void {
    const selectedCategory = event.target.value;
    this.category = this.categories.find((cat) => cat.CatId == selectedCategory);
    this.image =this.category.Image;   

    if (selectedCategory == "") {
      this.matchingServices = [];
    }
    this.matchingServices = this.services.filter(s => s.ServiceID.startsWith(selectedCategory.substring(0,4)));
  }
  serviceDetail(id: string): void {
    this.router.navigate(['/service-detail'],
      {
        queryParams: {
          ServiceID: id
        }
      });
  }
}
