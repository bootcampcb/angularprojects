import { Component, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Services } from '../models/services.model';
import { ServicesService } from '../providers/services.service';

@Component({
  selector: 'app-service-detail',
  templateUrl: './service-detail.component.html',
  styleUrls: ['./service-detail.component.css']
})
export class ServiceDetailComponent {
  service!: Services;
  ServiceID: string = "";
  picSrc: string = "";

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private ServicesService: ServicesService) { }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((params) => {
        this.ServiceID = params['ServiceID']});
    this.ServicesService.getService(this.ServiceID).subscribe((data) => {
      this.service = data;
      this.picSrc = "/assets/images/" + data.ServiceID + ".jpg";
    });
  }
  onClickReservationBtn(){
    this.router.navigate(['/reservation']);
  }
}
