import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Services } from '../models/services.model';

@Injectable({
  providedIn: 'root'
})
export class ServicesService {
  constructor(private http: HttpClient) { }
  
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Referrer-Policy': 'strict-origin-when-cross-origin',
    }),
  };

  getServices(): Observable<Services[]> {
    return this.http
      .get('http://localhost:8081/api/services/', this.httpOptions)
      .pipe(map((res) => <Services[]>res));
  }

  public getService(serviceID: string) {
    return this.http
      .get('http://localhost:8081/api/services/' + serviceID, this.httpOptions)
      .pipe(map((res) => <Services>res));
  }
}
