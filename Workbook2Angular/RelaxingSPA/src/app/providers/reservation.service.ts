import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Reservation } from '../models/reservation.model';

@Injectable({
  providedIn: 'root'
})
export class ReservationService {
  private endPoint: string = 'http://localhost:8081/api/reservations/';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Referrer-Policy': 'strict-origin-when-cross-origin',
    }),
  };

  constructor(private http: HttpClient) { }

  getReservations(): Observable<Reservation[]> {
    return this.http
      .get(this.endPoint, this.httpOptions)
      .pipe(map((res) => <Reservation[]>res));
  }

  postNewReservation(reservation: Reservation): Observable<Reservation> {
    return this.http.post<Reservation>(this.endPoint,
      {
        name: reservation.name,
        phone: reservation.phone,
        date: reservation.date,
        time: reservation.time
      },
      this.httpOptions);
  }
}