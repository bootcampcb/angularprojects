export class Services {
    constructor(public ServiceID: string, public ServiceName: string, 
        public Description: string, public Price: string,
        public Minutes: string, public CategoryName: string) {
        this.ServiceID = ServiceID;
        this.ServiceName = ServiceName;
        this.Description = Description;
        this.Price = Price;
        this.Minutes = Minutes;
        this.CategoryName = CategoryName;
    }
}