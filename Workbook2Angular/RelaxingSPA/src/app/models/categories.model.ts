export class Categories {
    constructor(public CategoryName: string, public CatId: string, public image: string) {
        this.CategoryName = CategoryName;
        this.CatId = CatId;
        this.image = image;
    }
}
