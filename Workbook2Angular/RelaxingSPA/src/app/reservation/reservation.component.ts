import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Reservation } from '../models/reservation.model';
import { ReservationService } from '../providers/reservation.service';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.css']
})
export class ReservationComponent {
  d = new Date();
  date = [
    this.d.getFullYear(),
    ('0' + (this.d.getMonth() + 1)).slice(-2),
    ('0' + this.d.getDate()).slice(-2)
  ].join('-');

  name: string = "";
  phone: string = "";
  time: string = "";

  constructor(private router: Router, private reservationService: ReservationService) {
  }

  onSubmit() {
    this.reservationService.postNewReservation(new Reservation(this.name, this.phone, this.date, this.time)).subscribe(data => console.dir(data));
    alert("Reservation submitted: We'll verify availability and call with a confirmation.")
    this.router.navigate(['/search']);
  }
}
