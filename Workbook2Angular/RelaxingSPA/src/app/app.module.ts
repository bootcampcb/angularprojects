import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SearchComponent } from './search/search.component';
import { DetailsComponent } from './details/details.component';
import { AutofocusFixModule } from 'ngx-autofocus-fix';

import { CategoriesService } from './providers/categories.service';
import { ReviewsService } from './providers/reviews.service';
import { ServicesService } from './providers/services.service';
import { ReservationService } from './providers/reservation.service';
import { ServiceDetailComponent } from './service-detail/service-detail.component';
import { ReviewAddComponent } from './review-add/review-add.component';
import { ReservationComponent } from './reservation/reservation.component';
import { FooterComponent } from './footer/footer.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { ApplicationComponent } from './application/application.component';
import { FormExerciseComponent } from './form-exercise/form-exercise.component';

const appRoutes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: "home", component: HomeComponent },
  { path: "search", component: SearchComponent },
  { path: "service-detail", component: ServiceDetailComponent},
  { path: "review-add", component: ReviewAddComponent},
  { path: "reservation", component: ReservationComponent},
  { path: "application", component: ApplicationComponent}
];


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SearchComponent,
    DetailsComponent,
    ServiceDetailComponent,
    ReviewAddComponent,
    ReservationComponent,
    FooterComponent,
    NavBarComponent,
    ApplicationComponent,
    FormExerciseComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    AutofocusFixModule.forRoot(),
    HttpClientModule,
    FontAwesomeModule
    
  ],
  providers: [CategoriesService, ReviewsService, ServicesService, ReservationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
