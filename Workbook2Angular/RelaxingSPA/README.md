# Chuck Borst's Relaxing Spa Project

## Description
Project Description
You will build an app using Angular (Single Page App (SPA)) that lets a visitor to a SPA 
RESORT explore the services offered at the premiere spa. You will use a Node.js/Express REST 
API to manage the data services.
The website must present to the user the following features: 
- A navigation bar with two buttons or links. One shows the user a "home page" with 
inviting information about the spa. The other takes the user to a "search page" where 
they can explore the services offered by the spa. 
- On the search page, users should be presented with a list of the types of services the spa 
offers. When the user clicks a type of service, they must see a list of services in that 
category. (ex: When you select "Massage and Bodywork", you see a list of all the 
massage-related services offered.) Don't display the services using tables on this page -
- instead use Bootstrap cards!
- When the user clicks a link next to a specific service, the site displays a "details page"
that shows the details of that service!
Technical Information
Your instructor will provide starter files for the server and the data it needs. 
You should be able to read the code in the server.js file and follow along with the logic for 
each API endpoint. The server supports the following requests
1) GET api/categories returns the types of spa services offered
2) GET api/services returns a list of all spa services offered
3) GET api/services/MASS6 returns information about the service whose id 
is MASS6
4) GET api/services/bycategory/HAIR returns the services offered in 
the spa's "Hair Salon", where HAIR is the category
5) GET api/reviews returns the last 3 user reviews of the SPA
6) POST api/reviews adds a user's review of the SPA
Because it’s a single page application, you will work with routing to create each "view" of the 
page dynamically.
2
FEEL FREE to edit the text in the categories and services JSON file, but DO NOT remove any 
fields or rename fields, otherwise server.js file will not work.
BONUS OPTION #1
Find an image for each category of service! Then modify categories.json to include the 
name of the image file (ex: massage.png). Use the image on the details page to make it 
more inviting! Make sure to put the image in the assets directory.
[
{"CategoryName": "Acupuncture", "CatId": "ACU", 
 "Image": "assets/acupuncture.png"},
{"CategoryName": "Body Treatments", "CatId": "BODY"
 "Image": "assets/body.png"},
 ...
]
BONUS OPTION #2
On the "home" page, display the last 3 reviews submitted to the spa. You will find them by 
calling GET api/reviews.
BONUS OPTION #3
Provide a "Review" link on the details page that allows the user to submit a review of the 
service. They fill out a form with their name and comments. When the user clicks a Submit 
button, you should POST the data to the API using api/reviews. 
(CHALLENGING) BONUS OPTION #4
Provide a "Make Reservation" link on the details page that allows the user to enter their name, 
a phone number, a date, and a requested time. (The spa will call them back with confirmation 
details.) When the user clicks a Submit button, you will POST the data to the API using 
api/reservation. The biggest trick to this bonus is the API endpoint doesn't exist. You 
will need to 1) add a reservations.json file and 2) code the POST handler for
api/reservations


```
git clone https://gitlab.com/bootcampcb/angularprojects.git 

```

## Home page
![Image](src/assets/images/homepage.png "icon")
## Search page
![Image](src/assets/images/search.png "icon")
## Details page
![Image](src/assets/images/details.png "icon")






