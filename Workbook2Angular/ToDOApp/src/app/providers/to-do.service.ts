import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ToDo } from '../models/to-do.model';


@Injectable({
  providedIn: 'root'
})
export class ToDoService {
  constructor(private http: HttpClient) { }
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Referrer-Policy': 'strict-origin-when-cross-origin',
    }),
  };

  getTodos(): Observable<ToDo[]> {
    return this.http
      .get('https://jsonplaceholder.typicode.com/todos/', this.httpOptions)
      .pipe(map((res) => <ToDo[]>res));
  }
  getToDoById(id: number): Observable<ToDo> {
    return this.http
      .get('https://jsonplaceholder.typicode.com/todos/'+ id, this.httpOptions)
      .pipe(map((res) => <ToDo>res));
  }
  postNewToDo(ToDo: ToDo): Observable<ToDo> {
    return this.http.post<ToDo>('https://jsonplaceholder.typicode.com/todos/',
      {
        userId: ToDo.userId,
        title: ToDo.title,
        completed: ToDo.completed
      },
      this.httpOptions);
  }
  updateToDo(ToDo: ToDo): Observable<ToDo> {
    return this.http.put<ToDo>('https://jsonplaceholder.typicode.com/todos/' + ToDo.id,
      {
        userId: ToDo.userId,
        title: ToDo.title,
        completed: ToDo.completed
      },
      this.httpOptions);
  }
  deleteToDo(ToDo: ToDo): Observable<ToDo> {
    return this.http.delete<ToDo>('https://jsonplaceholder.typicode.com/todos/' + ToDo.id,
      this.httpOptions);
  }
}
