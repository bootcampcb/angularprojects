import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToDo } from '../models/to-do.model';
import { ToDoService } from '../providers/to-do.service';

@Component({
  selector: 'app-new-to-do',
  templateUrl: './new-to-do.component.html',
  styleUrls: ['./new-to-do.component.css']
})
export class NewToDoComponent {
  userId: number = 0;
  title: string = "";
  completed: boolean = false;

  constructor(private activatedRoute: ActivatedRoute, private ToDoService: ToDoService) { 
    
  }

  onClickAddButton() {
    this.ToDoService.postNewToDo(new ToDo(this.userId, 0, this.title, this.completed)).subscribe(data => console.dir(data));
  }
}
