import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToDo } from '../models/to-do.model';
import { ToDoService } from '../providers/to-do.service';

@Component({
  selector: 'app-to-do-details',
  templateUrl: './to-do-details.component.html',
  styleUrls: ['./to-do-details.component.css']
})
export class ToDoDetailsComponent {
  todo!: ToDo;
  id: number = 0;
  userId: number = 0;
  title: string = "";
  completed: boolean = false;

  constructor(private activatedRoute: ActivatedRoute, private ToDoService: ToDoService) { }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.id = params['id']
    });
    this.ToDoService.getToDoById(this.id).subscribe((data) => {
      this.todo = data;
      this.userId = this.todo.userId;
      this.title = this.todo.title;
      this.completed = this.todo.completed;
    });

  }
  onClickUpdateButton() {
    this.ToDoService.updateToDo(new ToDo(this.userId, this.id, this.title, this.completed)).subscribe(data => console.dir(data));
  }  
  onClickDeleteButton() {
    this.ToDoService.deleteToDo(new ToDo(this.userId, this.id, this.title, this.completed)).subscribe(data => console.dir(data));
    alert("Record deleted");
  }
}
