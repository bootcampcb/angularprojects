import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToDo } from '../models/to-do.model';
import { ToDoService } from '../providers/to-do.service';

@Component({
  selector: 'app-to-dos',
  templateUrl: './to-dos.component.html',
  styleUrls: ['./to-dos.component.css']
})
export class ToDosComponent {
  todos: Array<ToDo> = [];

  constructor(private activatedRoute: ActivatedRoute, private ToDoService: ToDoService) { }

  ngOnInit(): void {
    this.ToDoService.getTodos().subscribe((data) => {
      this.todos = data;
    });
  }
}
