export class Course {
    constructor(public id: number, public dept: string, public courseNum: number, public courseName: string,
        public instructor: string, public startDate: string, public numDays: number) {
        this.id = id;
        this.dept = dept;
        this.courseNum = courseNum;
        this.courseName = courseName;
        this.instructor = instructor;
        this.startDate = startDate;
        this.numDays = numDays;
    }
}