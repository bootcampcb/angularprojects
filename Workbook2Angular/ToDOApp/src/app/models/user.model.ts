import { Address } from "./address.model";
import { Company } from "./company.model";
export class User {
    constructor(public id: number, public name: string, public username: string, public email: string,
        public address: Array<Address>, public phone: string, public website: string, public company: Array<Company>) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.email = email;
        this.address = address;
        this.phone = phone;
        this.website = website;
        this.company = company;
    }
}
