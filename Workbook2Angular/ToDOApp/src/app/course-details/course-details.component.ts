import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Course } from '../models/course.model';
import { CourseService } from '../providers/course.service';

@Component({
  selector: 'app-course-details',
  templateUrl: './course-details.component.html',
  styleUrls: ['./course-details.component.css']
})
export class CourseDetailsComponent {
  course!: Course;
  id: number = 0;

  constructor(private activatedRoute: ActivatedRoute, private CourseService: CourseService) { }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.id = params['id']});
    this.CourseService.getCourseById(this.id).subscribe((data) => {
      this.course = data;
    });
  }
}
