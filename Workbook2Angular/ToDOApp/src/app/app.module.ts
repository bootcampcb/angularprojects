import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { ToDosComponent } from './to-dos/to-dos.component';
import { HeaderComponent } from './header/header.component';
import { ToDoService } from './providers/to-do.service';
import { HomeComponent } from './home/home.component';
import { UsersComponent } from './users/users.component';
import { CoursesComponent } from './courses/courses.component';
import { CourseDetailsComponent } from './course-details/course-details.component';
import { NewToDoComponent } from './new-to-do/new-to-do.component';
import { ToDoDetailsComponent } from './to-do-details/to-do-details.component';
import { ConfirmDeleteComponent } from './confirm-delete/confirm-delete.component';
import { CourseAddComponent } from './course-add/course-add.component';

const appRoutes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: "home", component: HomeComponent },
  { path: "to-dos", component: ToDosComponent },
  { path: "to-do-details", component: ToDoDetailsComponent },
  { path: "new-to-do", component: NewToDoComponent },
  { path: "users", component: UsersComponent },
  { path: "courses", component: CoursesComponent },
  { path: "course-details", component: CourseDetailsComponent },
]

@NgModule({
  declarations: [
    AppComponent,
    ToDosComponent,
    HeaderComponent,
    HomeComponent,
    UsersComponent,
    CoursesComponent,
    CourseDetailsComponent,
    NewToDoComponent,
    ToDoDetailsComponent,
    ConfirmDeleteComponent,
    CourseAddComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    HttpClientModule,
  ],
  providers: [ToDoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
