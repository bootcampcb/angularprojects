import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from '../models/user.model';
import { UsersService } from '../providers/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent {
  users: Array<User> = [];

  constructor(private activatedRoute: ActivatedRoute, private UsersService: UsersService) { }

  ngOnInit(): void {
    this.UsersService.getUsers().subscribe((data) => {
      this.users = data;
    });
  }
}
