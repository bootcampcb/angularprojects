import { Component, OnInit } from '@angular/core';
import { Book } from '../models/book.model';

@Component({
  selector: 'app-library',
  templateUrl: './library.component.html',
  styleUrls: ['./library.component.css']
})
export class LibraryComponent implements OnInit {
  books : Array<Book>;

  constructor() {
    this.books = [ new Book("Project Hail Mary", "Andy Weir", 1)];
    this.books.push( new Book("Dune \(Anniversary\)", "Frank Hebert", 2),
    new Book("The Three-Body Problem", "Cixin Liu, Author, Ken Liu, Translator", 3),
    new Book("Station Eleven", "Emily St John Mandel", 4),
    new Book("The Running Man", " Stephen King", 5),
    );
  }

  ngOnInit(): void {

  }

}
