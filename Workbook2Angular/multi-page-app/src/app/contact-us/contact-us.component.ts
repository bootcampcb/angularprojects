import { Component } from '@angular/core';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent {
  message: string = 'under construction';
  name: string = 'John Smith';
  email: string = 'John.Smith@gmail.com';
  phone: string = '(555) 555-1234';
}
