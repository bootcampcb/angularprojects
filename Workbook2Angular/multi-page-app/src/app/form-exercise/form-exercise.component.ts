import { Component } from '@angular/core';
import { Contact } from '../models/contact.model';

@Component({
  selector: 'app-form-exercise',
  templateUrl: './form-exercise.component.html',
  styleUrls: ['./form-exercise.component.css']
})
export class FormExerciseComponent {
  newContact: Contact = new Contact("", "", "");
  constructor() {
  }
  onSubmit() {
    console.log(`Saving contact: ${JSON.stringify(this.newContact)}`);
    }
}
