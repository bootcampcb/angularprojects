import { Component } from '@angular/core';

@Component({
  selector: 'app-word-counter',
  templateUrl: './word-counter.component.html',
  styleUrls: ['./word-counter.component.css']
})
export class WordCounterComponent {
  someText: string = "";
  numWords: number = 0;
  numWords2: number = 0;

  wordCount(){
    this.numWords = this.someText.split(" ").length;
  }
  onFormSubmitted($event: Event) : void {
    this.numWords2 = this.someText.split(" ").length;
  }
    
}
