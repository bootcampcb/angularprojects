import { Component } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent {
  message: string = 'under construction';
  product1ImageURL: string = 'assets/chocolate_lab_cute_puppies.jpg';
  product1AltText: string = 'Chocolate lab puppy';
  product2ImageURL: string = 'assets/golden_retriever_cute_puppies.jpg';
  product2AltText: string = 'Golden retriever puppy';
  product3ImageURL: string = 'assets/siberian_husky_cute_puppies.jpg';
  product3AltText: string = 'Siberian husky puppy';
}
