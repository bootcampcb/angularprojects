import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayRandomComponent } from './display-random.component';

describe('DisplayRandomComponent', () => {
  let component: DisplayRandomComponent;
  let fixture: ComponentFixture<DisplayRandomComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DisplayRandomComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DisplayRandomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
