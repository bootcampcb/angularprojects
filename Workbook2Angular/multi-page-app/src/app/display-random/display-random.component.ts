import { Component } from '@angular/core';
import { RandomService } from '../providers/random.service';

@Component({
  selector: 'app-display-random',
  templateUrl: './display-random.component.html',
  styleUrls: ['./display-random.component.css']
})
export class DisplayRandomComponent {
  randomNumber: number = 0;
  min: number = 0;
  max: number = 0;
  constructor(private randomService: RandomService) {
  }
  
  onClickRandomButton() {
      this.randomNumber = this.randomService.getRandomNumber(this.min, this.max);
  }
}
