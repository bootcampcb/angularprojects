import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { SayingsService } from './providers/sayings.service';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ProductsComponent } from './products/products.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { UserInputComponent } from './user-input/user-input.component';
import { WordCounterComponent } from './word-counter/word-counter.component';
import { AutofocusFixModule } from 'ngx-autofocus-fix';
import { MathCalculatorComponent } from './math-calculator/math-calculator.component';
import { OutdoorActivitiesComponent } from './outdoor-activities/outdoor-activities.component';
import { LibraryComponent } from './library/library.component';
import { FormExerciseComponent } from './form-exercise/form-exercise.component';
import { SayingsComponent } from './sayings/sayings.component';
import { DisplayRandomComponent } from './display-random/display-random.component';
import { RandomService } from './providers/random.service';
import { MenuComponent } from './menu/menu.component';
import { MenuItemComponent } from './menu-item/menu-item.component';
import { PlayerComponent } from './player/player.component';
import { PlaylistComponent } from './playlist/playlist.component';

const appRoutes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: "home", component: HomeComponent },
  { path: "products", component: ProductsComponent },
  { path: "contact-us", component: ContactUsComponent },
  { path: "user-input", component: UserInputComponent },
  { path: "word-counter", component: WordCounterComponent },
  { path: "math-calculator", component: MathCalculatorComponent },
  { path: "outdoor-activities", component: OutdoorActivitiesComponent },
  { path: "library", component: LibraryComponent },
  { path: "form-exercise", component: FormExerciseComponent },
  { path: "sayings", component: SayingsComponent },
  { path: "display-random", component: DisplayRandomComponent },
  { path: "menu", component: MenuComponent },
  { path: "menu-item", component: MenuItemComponent },
  { path: "playlist", component: PlaylistComponent },
  { path: "player", component: PlayerComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProductsComponent,
    ContactUsComponent,
    UserInputComponent,
    WordCounterComponent,
    MathCalculatorComponent,
    OutdoorActivitiesComponent,
    LibraryComponent,
    FormExerciseComponent,
    SayingsComponent,
    DisplayRandomComponent,
    MenuComponent,
    MenuItemComponent,
    PlayerComponent,
    PlaylistComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    AutofocusFixModule.forRoot(),
  ],
  providers: [SayingsService, RandomService],
  bootstrap: [AppComponent]
})
export class AppModule { }
