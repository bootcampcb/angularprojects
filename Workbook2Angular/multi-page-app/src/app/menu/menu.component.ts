import { Component,OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuService } from '../providers/menu.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  menuItems: Array<any> = [];
  constructor(private router: Router, private menuService: MenuService) {}

  ngOnInit() { 
    this.menuItems = this.menuService.getMenu();
  }

  menuDetails(id: string): void {
    this.router.navigate(['/menu-item'],
      {
        queryParams: {
          id: id
        }
      });
  }
}