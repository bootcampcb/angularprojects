import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PlaylistService } from '../providers/playlist.service';

@Component({
  selector: 'app-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.css']
})
export class PlaylistComponent implements OnInit {

  constructor(private router: Router, private playService: PlaylistService) {}
  songs: Array<any> = [];
  ngOnInit() { 
    this.songs = this.playService.getSongs();
  }

  playSong(id: string): void {
    this.router.navigate(['/player'],
      {
        queryParams: {
          id: id
        }
      });
  }

}