import { Injectable } from '@angular/core';
import { Menu } from '../models/menu.model';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  menuItems: Array<Menu>;
  constructor() { 
    this.menuItems = [
      {
        id: 1, name: 'Burger', description: 'A classic hamburger',
        price: 5.99
      },
      {
        id: 2, name: 'Fries', description: 'Crispy french fries',
        price: 2.99
      },
      {
        id: 3, name: 'Soda', description: 'A cold soda', price: 1.99
      }
    ];
  }

  public getMenu(): Array<Menu> {
    return this.menuItems;
  }
  public getMenuItem(id: number) {
    return this.menuItems.find((menuItems) => menuItems.id == id);
  }
}
