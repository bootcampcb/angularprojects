import { Injectable } from '@angular/core';
import { Saying } from '../models/saying.model'

@Injectable({
  providedIn: 'root'
})
export class SayingsService {
  categories: Array<string>;
  sayings: Array<Saying>;
  constructor() {
    // load the sayings categories
    this.categories = ['Inspiration', 'Staying Safe', 'Statistics'];
    // load the sayings
    this.sayings = [
      {
        category: 'Inspiration',
        person: 'A.A. Milne',
        quote:
          "You're braver than you believe, and stronger than you seem, and smarter than you think.",
      },
      {
        category: 'Inspiration',
        person: 'Henry Ford',
        quote:
          'Nothing is particularly hard if you break it down into small jobs.',
      },
      {
        category: 'Staying Safe',
        person: 'Benjamin Franklin',
        quote: 'An apple a day keeps the doctor away.',
      },
      {
        category: 'Staying Safe',
        person: 'John A. Shedd',
        quote:
          'A ship in a harbor is safe, but that is not what ships are for.',
      },
      {
        category: 'Statistics',
        person: 'Andrew Lang',
        quote:
          'He uses statistics as a drunken man uses lamp posts... for support rather than illumination.',
      },
    ];

  }
  public getCategories(): Array<string> {
    return this.categories;
  }
  public getSayings(): Array<Saying> {
    return this.sayings;
  }
  public getSayingsThatMatchCategory(category: string): Array<Saying> {
    let matching = this.sayings.filter(s => s.category == category);
    return matching;
  }
  public getSayingsThatMatchPerson(person: string) {
    let matching = this.sayings.filter(s => s.person == person);
    return matching;
  }
}