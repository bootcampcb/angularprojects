export class Saying {
    constructor(public category: string, public person: string, public quote: string) {
        this.category = category;
        this.person = person;
        this.quote = quote;
    }
}
