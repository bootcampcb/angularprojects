export class Book {

    constructor(public title: string, public author: string, public ISBN: number) {
        this.title = title;
        this.author = author;
        this.ISBN = ISBN;
    }
}
