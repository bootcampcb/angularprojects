export class Playlist {
    constructor(public id: number, public name: string, public artist: string, public src: string) {
        this.id = id;
        this.name = name;
        this.artist = artist;
        this.src = src;
    }
}
