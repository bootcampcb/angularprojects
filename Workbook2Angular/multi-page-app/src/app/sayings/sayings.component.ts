import { Component } from '@angular/core';
import { SayingsService } from '../providers/sayings.service';

@Component({
  selector: 'app-sayings',
  templateUrl: './sayings.component.html',
  styleUrls: ['./sayings.component.css'],
})
export class SayingsComponent {
  title = 'Sayings';
  personSelected: string = "";
  // raw data
  categories: Array<string> = [];
  sayings: Array<any> = [];
  // sayings that match a selection
  matchingSayings: Array<any> = [];
  constructor(private sayingsService: SayingsService) {
    // set up for when page loads and no sayings show
    this.matchingSayings = [];
  }

  ngOnInit() {
    this.categories = this.sayingsService.getCategories();
  }
  onSelectSaying(event: any): void {
    const selectedCategory = event.target.value;
    // if they pick the select one option... show nothing
    if (selectedCategory == "") {
      this.matchingSayings = [];
    }
    else {
      // otherwise... find the matching sayings and show
      this.matchingSayings = this.sayingsService.getSayingsThatMatchCategory(selectedCategory);
    }
  }
  getSayingsByPerson() {
    this.matchingSayings = this.sayingsService.getSayingsThatMatchPerson(this.personSelected);
    this.personSelected = "";
  }
}

