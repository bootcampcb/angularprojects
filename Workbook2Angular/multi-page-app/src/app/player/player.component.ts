import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { PlaylistService } from '../providers/playlist.service';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css'],
})
export class PlayerComponent {
  song!: any;
  url!: SafeResourceUrl;
  songs = this.playService.songs;

  constructor(
    private activatedRoute: ActivatedRoute,
    private sanitizer: DomSanitizer,
    private playService: PlaylistService) {

  }
  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params) => {
      let id = params['id'];
      this.song = this.playService.getSong(id);
      this.url =
        this.sanitizer.bypassSecurityTrustResourceUrl(this.song.src);
    });
  }
}