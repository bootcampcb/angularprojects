import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MenuService } from '../providers/menu.service';

@Component({
  selector: 'app-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.css']
})
export class MenuItemComponent {
  menuItem!: any;
  constructor(private activatedRoute: ActivatedRoute, private menuService: MenuService) {}

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params) => {
      let id = params['id'];
      this.menuItem = this.menuService.getMenuItem(id);
    });
  }
}
