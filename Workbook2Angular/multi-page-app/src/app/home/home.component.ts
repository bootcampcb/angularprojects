import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  message: string = 'under construction';
  intro: string = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras consequat nec turpis vitae sollicitudin. Mauris viverra enim aliquet ligula elementum accumsan. Nulla accumsan dignissim justo, vitae imperdiet est aliquam non. Fusce sit amet turpis non mauris ultrices elementum vel cursus eros. Pellentesque sit amet auctor massa. Integer ultricies massa quis eros iaculis molestie in id leo. Proin a erat justo. Pellentesque condimentum dictum est, et pulvinar erat luctus at. Curabitur rhoncus tellus a ornare ultricies. Aliquam a semper lacus. Maecenas purus nisi, volutpat vitae erat ac, laoreet ultrices nisl.";
  details: string = "Vestibulum elementum lacinia nibh. Vestibulum id efficitur sapien. Aenean quam ante, lobortis sit amet arcu vitae, consequat congue justo. Donec dictum nibh non nisl laoreet pharetra. Cras quis laoreet orci. Nullam nisl nisl, condimentum eu est id, finibus vestibulum augue. Nullam dictum consectetur est, id luctus neque eleifend eu.";
  conclusion: string = "Maecenas ut mi lectus. Praesent et mi risus. Cras at dolor faucibus, aliquam nunc vehicula, congue magna. Donec aliquam faucibus nisi, ac egestas sapien ullamcorper nec. Cras ipsum diam, porta et finibus ut, aliquam mattis lectus. Cras aliquam risus quis efficitur venenatis. In sit amet aliquam urna. Aliquam in dolor suscipit metus scelerisque dapibus. Mauris sed sem at tellus blandit eleifend. Praesent ultrices arcu lectus. Aliquam eget turpis vel nulla porta tincidunt sit amet nec odio. Morbi vitae volutpat felis, ut auctor orci.";
}
