import { Component,OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  menuItems = [
    {
      id: 1, name: 'Burger', description: 'A classic hamburger',
      price: 5.99
    },
    {
      id: 2, name: 'Fries', description: 'Crispy french fries',
      price: 2.99
    },
    {
      id: 3, name: 'Soda', description: 'A cold soda', price: 1.99
    }
  ];
  constructor(private router: Router) {}

  ngOnInit() { }

  menuDetails(id: string): void {
    this.router.navigate(['/menuItem'],
      {
        queryParams: {
          id: id
        }
      });
  }
}