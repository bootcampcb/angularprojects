import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = "Chuck's new-project";
  msg = 'This is my message'
  today: string = "";
  greeting: string = "";

  items = [
    { id: 1, name: 'Item 1', price: 10.99 },
    { id: 2, name: 'Item 2', price: 19.99 },
    { id: 3, name: 'Item 3', price: 4.99 },
    { id: 4, name: 'Item 4', price: 8.99 }
    ];
    filterText = '';
    sortField = '';
    sortOrder = '';

  ngOnInit() {
    let rightNow = new Date();
    this.today = rightNow.toLocaleDateString();

    if (rightNow.getHours() >= 5 && rightNow.getHours() <= 11) {
      this.greeting = "It's a wonderful morning";
    } else if (rightNow.getHours() >= 12 && rightNow.getHours() <= 17) {
      this.greeting = "It's a great afternoon";
    } else if (rightNow.getHours() >= 18 && rightNow.getHours() <= 23) {
      this.greeting = "It's a nice evening";
    } else if (rightNow.getHours() >= 0 && rightNow.getHours() <= 4) {
      this.greeting = "It's deep night -- why are you awake";
    }
  }

  playGame(name: string) {
    console.log(`Hi ${name}, want to play a game?`);
  }

}
