import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'hello-world';
  name = 'Centene';
  centeneVideoLink = "https://www.youtube.com/embed/F22Xqbk9n4A?rel=0";
}
